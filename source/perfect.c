#include <stdio.h>
#include <stdlib.h>
#define NUMBER 100000000

void Eratosthenes(int *heap){
  int i, j; // カウント変数

  heap[i] = {}; // 全てに0をセット

  for(i=2; i<NUMBER;){
    for(j=i+i; j<NUMBER; j+=i) {
      heap[j]++; //素数でない値に+1していく
		}
    while(heap[i++] != 0) {
			continue;
		}
  }
  for(i = 2; i < NUMBER; i *= 2) {
    heap[i-1]--; // i=2^nなのでそれの一つ前の番地から1を引く
	}
}

int main() {
  int *heap;
  unsigned int cnt;
  unsigned int per;

  heap  = (int *)malloc(sizeof(int) * NUMBER); // 1億までの配列をヒープをつかってつくる
  Eratosthenes(heap);

  printf("%d未満の完全数\n", NUMBER);
  for(cnt=0; cnt*(cnt+1)/2<NUMBER; cnt++){
    if(heap[cnt] == -1){ // メルセンヌ素数は-1になっているはず
      per = cnt*(cnt+1)/2; // 完全数を求める計算
      printf("%u\n", per);
    }
  }
  return 0;
}
