/**
 * square.c
 *
 * 入力された4つの値を辺とする四角形が存在するか判定するプログラム
 *
 * @copyright	&copy;2015 Ryosuke Hagihara <raryosu@gmail.com>
 * @create		2015.08.11
 * @version		1.1.0811
 */

#include <stdio.h>
#define SIDES 4

int isSquare(int *side) {
	int i, j; // カウント変数
	int temp;
	for(i=0; i<SIDES; i++) {
		// 1辺でも0の辺が含まれていた場合
		if(side[i]==0) {
			return 0;
		}

		// ソート
		for (j=i+1; j<SIDES; ++j) {
			if (side[i] < side[j]) {
				temp =  side[i];
				side[i] = side[j];
				side[j] = temp;
			}
		}
	}

	// 辺の和から最大の辺の長さを引く
	if(side[0]-side[3] < side[1]+side[2]) {
		return 1;
	} else {
		return 0;
	}
}

int main() {
	int count; // カウント変数
	int flag; // 判定結果保持変数
	int side[SIDES]; // 4辺の長さ

	/*
		 入力を促す
		 */
	printf("%d辺の長さを半角スペース区切りで入力してください: ", SIDES);
	for(count=0; count<SIDES; count++) {
		scanf("%d", &side[count]);
	}

	/*
		 判定関数に判定してもらう
		 */
	flag = isSquare(side);

	/*
		 結果表示
		 */
	for(count=0; count<SIDES; count++) {
		printf("%d", side[count]);
		printf((count!=SIDES-1) ? ", " : "");
	}
	printf("を%d辺とする四角形は", SIDES);
	printf(flag ? "存在します。\n" : "存在しません。\n");

	return 0;

}
