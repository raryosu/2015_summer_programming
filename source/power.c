#include <stdio.h>
#include <math.h>

/**
 * calc_log
 * logを取る
 *
 * @param		double		root	logを取りたい値
 * @return	double
 */
double calc_log(double root) {
	return log10(root);
}

int main() {
	double a, b; // 入力
	double index;
	double index_sub;

	printf("2数を入力してください(a b): ");
	scanf("%lf%lf", &a, &b);

	// log10(a)
	index = calc_log(a);

	// log10(a^b) = b log10(a)
	index *= b;
	index_sub = index;

	// intにキャスト
	index = (int)index;

	if(index_sub != (double)index) {
		index++;
	}

	printf("%f^%fは%.0f桁です\n", a, b, index);

}
